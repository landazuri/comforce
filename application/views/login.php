<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        .yo{
            margin-left: 60px;
        }
        .ancho{
            width: 300px !important;
        }
        .alto{
        margin-top: 60px;
        }
        .fml{
            margin-top: 80px !important;
        }
    </style>
    <?php include 'inc/css.php'; ?>

    <div class="lock-word animated fadeInDown">
        <span class="first-word">INICIAR </span><span class="yo">SESIÓN</span>
    </div>
    <div class="middle-box  lockscreen animated fadeInDown ancho">
        <center>
            <div class="m-b-md alto">
                <img alt="image"  src="<?= base_url()?>plantilla/images/logo-header.png">
            </div>
        </center>
        <form class="m-t fml" role="form" method="post" action="">
            <div class="form-group">
                <label>USUARIO</label> 
                <input type="text" name="cc" class="form-control " required>
            </div>
            <div class="form-group">
                <label>CONTRASEÑA</label>
                <input type="password" name="pass" class="form-control " required>
            </div>
            <button type="submit" class="btn btn-success block full-width">INGRESAR</button>
        </form>

    </div>

</body>

<?php include 'inc/js.php'; ?>

<script>
    function valida(e){
        tecla = (document.all) ? e.keyCode : e.which;

            //Tecla de retroceso para borrar, siempre la permite
            if (tecla==8){
                return true;
            }
            
            // Patron de entrada, en este caso solo acepta numeros
            patron =/[0-9]/;
            tecla_final = String.fromCharCode(tecla);
            return patron.test(tecla_final);
        }
    </script>



    </html>

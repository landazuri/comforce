<!DOCTYPE html>
<html>
<head>
    <?php require_once 'inc/css.php'; ?>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <form method="POST" action="">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element"> <span>
                                <img alt="image" class="img-circle" width="180" src="<?= base_url()?>plantilla/images/logo-header.png" />
                                <span class="clear"><span class="block m-t-xs"> <strong class="font-bold"><?php echo $nombre; ?></strong></span>
                            </div> 
                        </li>
                        <center><button class="btn btn-success " name="registrar" type="sudmit"><i class="fa fa-upload"></i>&nbsp;&nbsp;<span class="bold">CONSECUTIVO</span></button></center>
                    </ul>
                </form>
            </div> 
        </nav>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header"> 
                    </div>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>ADMINISTRACIÓN COMFORCE</li>
                        <li>
                            <a href="<?= base_url() ?>welcome/salir">
                                <i class="fa fa-sign-out"></i> Cerrar sesión
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
            <br>
            <div class="wrapper wrapper-content" >
                <div class="animated rollIn">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content text-center p-md">

                                    <h1><span class="text-navy">BIENVENIDO.!</span></h1>
                                    <h2><br> PARA NOSOTROS ES UN GUSTO QUE ESTÉS AQUÍ </h2>
                                </div>
                            </div>
                        </div>                      
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <div class="ibox-content text-center p-md">
                                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>DESCRIPCION</th>
                                                <th>FECHA</th>
                                                <th>PRESUPUESTO</th>
                                                <th>USUARIO</th>
                                                <th>SEDE</th>
                                                <th>ACCION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($formulario as $dt) { ?>

                                                <tr>
                                                    <td><?= $dt->idformulario ?></td>
                                                    <td><?= $dt->descripcion ?></td>
                                                    <td><?= $dt->fechacreacion ?></td>
                                                    <td><?= $dt->presupuesto ?></td>
                                                    <td><?= $dt->nombre ?></td>
                                                    <td><?= $dt->sede ?></td>
                                                    <form method="post" action="">
                                                        <td>
                                                            <center>
                                                                <?php if (!empty($dt->descripcion) || !empty($dt->fechacreacion)) { ?>
                                                                    <button class="btn btn-warning btn-xs" data-toggle="modal" type="button" href="#modal<?= $dt->idformulario; ?>"><i class="fa fa-pencil"></i> <span class="bold"></span></button>
                                                                <?php }else{  ?>
                                                                    <button class="btn btn-info btn-xs" data-toggle="modal" type="button" href="#modal2<?= $dt->idformulario; ?>"><i class="fa fa-wrench"></i> <span class="bold"></span></button>
                                                                <?php } ?>
                                                            </center>
                                                        </td>
                                                    </form>
                                                </tr>
                                                <div id="modal2<?= $dt->idformulario; ?>" class="modal fade" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog">
                                                        <form method="POST"  action="" >
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                 <div class="col-lg-12">
                                                                    <center>
                                                                        <h2>Consecutivo <?= $dt->idformulario; ?></h2>
                                                                        <input type="text" hidden value="<?= $dt->idformulario; ?>" name="id">
                                                                    </center>
                                                                    <br> 
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-6">
                                                                        <label>SEDE</label>
                                                                        <select class="form-control m-b" name="sede">
                                                                            <option value="">Selecione una sede.</option>
                                                                            <?php foreach ($sede as $dt2) { ?>
                                                                                <option value="<?= $dt2->idsede ?>"><?= $dt2->descripcion ?></option>
                                                                            <?php } ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-lg-6">
                                                                        <div class="form-group">
                                                                            <label>PRESUPUESTO</label>
                                                                            <input type="number" name="presupuesto" class="form-control numero">
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <label>DESCRIPCION</label> 
                                                                            <textarea rows="4" cols="50" name="obs" class="form-control" required=""></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="sudmit" name="actualizar" class="btn btn-success">GUARDAR</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div id="modal<?= $dt->idformulario; ?>" class="modal fade" aria-hidden="true" style="display: none;">
                                                <div class="modal-dialog">
                                                    <form method="POST"  action="" >
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                             <div class="col-lg-12">
                                                                <center>
                                                                    <h2>Consecutivo <?= $dt->idformulario; ?></h2>
                                                                    <input type="text" hidden value="<?= $dt->idformulario; ?>" name="id">
                                                                </center>
                                                                <br> 
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <label>SEDE</label>
                                                                    <select class="form-control m-b" name="sede">
                                                                        <option value="<?= $dt->idsede; ?>"><?= $dt->sede; ?></option>

                                                                        <?php foreach ($sede as $dt2) { 
                                                                            if ($dt->idsede != $dt2->idsede) { ?>

                                                                             <option value="<?= $dt2->idsede ?>"><?= $dt2->descripcion ?></option>

                                                                         <?php } } ?>
                                                                     </select>
                                                                 </div>
                                                                 <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <label>PRESUPUESTO</label>
                                                                        <input type="number" name="presupuesto" value="<?= $dt->presupuesto; ?>" class="form-control numero">
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <label>OBSERVACIONES</label> 
                                                                        <textarea rows="4" cols="50" name="obs" class="form-control" required=""><?= $dt->descripcion; ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="sudmit" name="actualizar" class="btn btn-success">ACTUALIZAR</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    <?php }  ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br><br><br><br>
        <div class="footer">
            <div class="row">
                <div class="pull-right"> 
                    <small>Copyright © <script>document.write(new Date().getFullYear())</script> </small>
                </div>
                <div >
                    COMFORCE
                </div>
            </div>
        </div>
    </div>
</div>

<?php  require_once 'inc/js.php'; ?>
<script>
    $(document).ready(function(){
        $('.dataTables-example').DataTable({
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
            { extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},

            {extend: 'print',
            customize: function (win){
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');

                $(win.document.body).find('table')
                .addClass('compact')
                .css('font-size', 'inherit');
            }
        }
        ]

    });

    });


</script>
</body>
</html>

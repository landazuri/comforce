<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>COMFORCE</title>
<link rel="shortcut icon" type="image/x-icon" href="<?= base_url()?>plantilla/images/logo-header.png">
<link href="<?= base_url()?>plantilla/css/bootstrap.min.css" rel="stylesheet">
<link href="<?= base_url()?>plantilla/font-awesome/css/font-awesome.css" rel="stylesheet">
<!-- Toastr style -->
<link href="<?= base_url()?>plantilla/css/plugins/toastr/toastr.min.css" rel="stylesheet">
<!-- Gritter -->
<link href="<?= base_url()?>plantilla/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">
<link href="<?= base_url()?>plantilla/css/animate.css" rel="stylesheet">
<link href="<?= base_url()?>plantilla/css/style.css" rel="stylesheet">
<link href="<?= base_url()?>plantilla/css/plugins/select2/select2.min.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="<?= base_url()?>plantilla/css/plugins/steps/jquery.steps.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/cropper/cropper.min.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/switchery/switchery.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
<link href="<?= base_url()?>plantilla/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">

<link href="<?= base_url()?>plantilla/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

<!-- Sweet Alert -->
<link href="<?= base_url()?>plantilla/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">

<style type="text/css">
  .yolo{
    margin-top: -17px !important;
  }
  .opciones{
    margin-top: 9px !important;
  }
  .a{
    margin-top: 310px !important;
    margin
  }
  #label1{
    margin-left: -14px;
  }
  .espaciomodal{
    margin-top: 200px;
  }
  .anchomodal{
    width: 1100px;
  }

  #finalizado{
    background-color: #ffffff !important;

  }

  .gris{
    background-color: #dddddd;
  }

  .azulmarino{

    background-color: #e6ee9c
    

  }


  .texto {
    font-size:28px;
    font-family:helvetica;
    font-weight:bold;
    color:#673ab7;
    text-transform:uppercase;
  }
  .parpadea {

    animation-name: parpadeo;
    animation-duration: 1s;
    animation-timing-function: linear;
    animation-iteration-count: infinite;

    -webkit-animation-name:parpadeo;
    -webkit-animation-duration: 1s;
    -webkit-animation-timing-function: linear;
    -webkit-animation-iteration-count: infinite;
  }

  @-moz-keyframes parpadeo{  
    0% { opacity: 1.0; }
    50% { opacity: 0.0; }
    100% { opacity: 1.0; }
  }

  @-webkit-keyframes parpadeo {  
    0% { opacity: 1.0; }
    50% { opacity: 0.0; }
    100% { opacity: 1.0; }
  }

  @keyframes parpadeo {  
    0% { opacity: 1.0; }
    50% { opacity: 0.0; }
    100% { opacity: 1.0; }
  }

  .footer{
    margin-top: 60px !important;
  }

</style>


</head>

<body class="skin-1 pace-doner"  background="<?= base_url()?>plantilla/images/fondo1.png">


<!-- Mainly scripts -->
<script src="<?= base_url()?>plantilla/js/jquery-3.1.1.min.js"></script>
<script src="<?= base_url()?>plantilla/js/bootstrap.min.js"></script>
<script src="<?= base_url()?>plantilla/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?= base_url()?>plantilla/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Flot -->
<script src="<?= base_url()?>plantilla/js/plugins/flot/jquery.flot.js"></script>
<script src="<?= base_url()?>plantilla/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
<script src="<?= base_url()?>plantilla/js/plugins/flot/jquery.flot.spline.js"></script>
<script src="<?= base_url()?>plantilla/js/plugins/flot/jquery.flot.resize.js"></script>
<script src="<?= base_url()?>plantilla/js/plugins/flot/jquery.flot.pie.js"></script>

<!-- Peity -->
<script src="<?= base_url()?>plantilla/js/plugins/peity/jquery.peity.min.js"></script>
<script src="<?= base_url()?>plantilla/js/demo/peity-demo.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?= base_url()?>plantilla/js/inspinia.js"></script>
<script src="<?= base_url()?>plantilla/js/plugins/pace/pace.min.js"></script>

<!-- jQuery UI -->
<script src="<?= base_url()?>plantilla/js/plugins/jquery-ui/jquery-ui.min.js"></script>

<!-- GITTER -->
<script src="<?= base_url()?>plantilla/js/plugins/gritter/jquery.gritter.min.js"></script>

<!-- Sparkline -->
<script src="<?= base_url()?>plantilla/js/plugins/sparkline/jquery.sparkline.min.js"></script>

<!-- Sparkline demo data  -->
<script src="<?= base_url()?>plantilla/js/demo/sparkline-demo.js"></script>


<!-- Select2 -->
<script src="<?= base_url()?>plantilla/js/plugins/select2/select2.full.min.js"></script>

<!-- ChartJS-->
<script src="<?= base_url()?>plantilla/js/plugins/chartJs/Chart.min.js"></script>

<!-- Toastr -->
<script src="<?= base_url()?>plantilla/js/plugins/toastr/toastr.min.js"></script>

<!-- Steps -->
<script src="<?= base_url()?>plantilla/js/plugins/steps/jquery.steps.min.js"></script>

<!-- Jquery Validate -->
<script src="<?= base_url()?>plantilla/js/plugins/validate/jquery.validate.min.js"></script>

<!-- Chosen -->
<script src="<?= base_url()?>plantilla/js/plugins/chosen/chosen.jquery.js"></script>

<!-- JSKnob -->
<script src="<?= base_url()?>plantilla/js/plugins/jsKnob/jquery.knob.js"></script>

<!-- Input Mask-->
<script src="<?= base_url()?>plantilla/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<!-- Data picker -->
<script src="<?= base_url()?>plantilla/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- NouSlider -->
<script src="<?= base_url()?>plantilla/js/plugins/nouslider/jquery.nouislider.min.js"></script>

<!-- Switchery -->
<script src="<?= base_url()?>plantilla/js/plugins/switchery/switchery.js"></script>

<!-- IonRangeSlider -->
<script src="<?= base_url()?>plantilla/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

<!-- iCheck -->
<script src="<?= base_url()?>plantilla/js/plugins/iCheck/icheck.min.js"></script>

<!-- Color picker -->
<script src="<?= base_url()?>plantilla/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- Clock picker -->
<script src="<?= base_url()?>plantilla/js/plugins/clockpicker/clockpicker.js"></script>

<!-- Image cropper -->
<script src="<?= base_url()?>plantilla/js/plugins/cropper/cropper.min.js"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script src="<?= base_url()?>plantilla/js/plugins/fullcalendar/moment.min.js"></script>

<!-- Date range picker -->
<script src="<?= base_url()?>plantilla/js/plugins/daterangepicker/daterangepicker.js"></script>

<!-- TouchSpin -->
<script src="<?= base_url()?>plantilla/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

<!-- Tags Input -->
<script src="<?= base_url()?>plantilla/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<!-- Dual Listbox -->
<script src="<?= base_url()?>plantilla/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>

<!-- Data tables -->
<script src="<?= base_url()?>plantilla/js/plugins/dataTables/datatables.min.js"></script>

<!-- Sweet alert -->
<script src="<?= base_url()?>plantilla/js/plugins/sweetalert/sweetalert.min.js"></script>




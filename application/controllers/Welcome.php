<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	
	public function index(){
		if ($this->session->userdata('comforce')) {

			header("location:".base_url()."bienvenido");

		}else{

			if ($this->input->server('REQUEST_METHOD')=='POST') {

				$idUsuario= $this->input->post('cc'); 
				$passwor= $this->input->post('pass'); 

				$this->load->model("M_Usuario");
				$fila = $this->M_Usuario->verificar_usuario($idUsuario);

				if ($fila!= null) {

					$data = array();
					$data ["usuario"] = $fila;

					print_r($data);

					foreach ($data as $key => $value) {
						if (($value["password"] == $passwor)){

							$data = array(
								'cc' => $value["idUsuario"],
								'nom' => $value["nombre"],
								'comforce'=> true );
							$this->session->set_userdata($data);

							header("location:".base_url()."bienvenido");

						}else{
							header("location:".base_url());
						}
					}

				}else{
					header("location:".base_url());
				}

			}

			$this->load->view('login');

		}


	}

	public function salir(){
		$this->load->model("M_Usuario");
		$this->session->sess_destroy();
		header("location:".base_url());

	}

	public function llegada(){
		if ($this->session->userdata('comforce')) {


			if (isset($_POST['registrar'])) {
			}


			if (isset($_POST['actualizar'])) {
				$this->load->model("M_Formulario");

				date_default_timezone_set("America/Bogota"); $fecha=date("Y-m-d");

				$datos = array();
				$datos ['idUsuario']= $this->session->userdata('cc');
				$datos ['descripcion']=$_POST['obs'];
				$datos ['fechacreacion']=$fecha;
				$datos ['presupuesto']= number_format($_POST['presupuesto']) ;
				$datos ['idsede']=$_POST['sede'];

				if ($this->M_Formulario->Actualizar($_POST['id'],$datos)>0) {
					$vista ["mensaje"] = 'success';
					$vista ["texto"] = 'Se actualizo con exito.!';
				}else{
					$vista ["mensaje"] = 'error';
					$vista ["texto"] = 'No se actualizo.!';
				}
			}

			if (isset($_POST['borrar'])) {
				$this->load->model("M_Formulario");
				if ($this->M_AsignacionTrailer->borrar($_POST['id'])>0) {
					$vista ["mensaje"] = 'success';
					$vista ["texto"] = 'Se borro con exito.!';
				}else{
					$vista ["mensaje"] = 'error';
					$vista ["texto"] = 'No se borro.!';
				}
			}

			if (isset($_POST['registrar'])) {
				$this->load->model("M_Formulario");
				$numero = $this->M_Formulario->ultimo();

				$consecutivo = $numero['idformulario'] + 1;

				$datos = array();
				$datos ['idformulario']= $consecutivo;

				//print_r($datos);

				if ($this->M_Formulario->insertar($datos)>0) {
					$vista ["mensaje"] = 'success';
					$vista ["texto"] = 'Se creo con exito.!';
				}else{
					$vista ["mensaje"] = 'error';
					$vista ["texto"] = 'No se creo.!';
				}
			}

			$this->load->model("M_Formulario");
			$vista = array();
			$vista["idUsuarioPed"] = $this->session->userdata('cc');
			$vista ["nombre"] = $this->session->userdata('nom');
			$vista ["formulario"] = $this->M_Formulario->listar();
			$vista ["sede"] = $this->M_Formulario->listarsede();
			$this->load->view('inicio',$vista);
		}else{
			header("location:".base_url());
		}
	}




}

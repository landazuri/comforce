<?php 


/**
* 
*/
class M_Formulario extends Ci_Model{
	
	public function __contruct(){
		parent::__contruct();
	}

	private $tabla = "formulario";
	private $tablasede = "sede";

	public function insertar($datos){
		return $this->db->insert($this->tabla,$datos);
	}


	public function listar(){
		$resultado = $this->db->query("SELECT fo.idformulario,fo.descripcion,fo.fechacreacion,fo.presupuesto,us.nombre,se.descripcion as sede , fo.idsede  FROM formulario fo 
			LEFT JOIN usuario us on us.idUsuario = fo.idUsuario
			LEFT JOIN sede se on se.idsede = fo.idsede");
		return $resultado->result();
	}
	public function ultimo(){
		$resultado = $this->db->query("SELECT `idformulario` FROM `formulario`
			ORDER BY `idformulario` DESC
			LIMIT 1");
		return $resultado->row_array();
	}


	public function listarsede(){
		$resultado = $this->db->get($this->tablasede);
		return $resultado->result();
	}

	public function Actualizar($id,$datos){
		$this->db->where("idformulario",$id);
		return $this->db->update($this->tabla,$datos);
	}

	public function borrar($id){
		$this->db->where("idformulario",$id);
		return $this->db->delete($this->tabla);
	}

}


?>